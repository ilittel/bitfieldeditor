package org.bfe.model;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * A consecutive range of bits that can be given a name.
 */
public class BitField implements Comparable<BitField>, Serializable {
    private static final long serialVersionUID = -6651959760389248802L;

    private final int startBitPos;
    private final int endBitPos;
    private String name;

    /**
     * Constructor.
     *
     * @param startBitPos Position of first bit in range.
     * @param endBitPos Position of last bit in range.
     * @throws IllegalArgumentException if startPos is greater than endPos.
     */
    public BitField(int startBitPos, int endBitPos) {
        if (startBitPos > endBitPos) {
            throw new IllegalArgumentException("Start bit position cannot be higher than end bit position");
        }

        this.startBitPos = startBitPos;
        this.endBitPos = endBitPos;
        this.name = new String("Bit " + startBitPos + "..." + endBitPos);
    }

    @Override
    public int compareTo(BitField o) {
        if (this.getStartBitPos() == o.getStartBitPos() && this.getEndBitPos() == o.getEndBitPos()) {
            return 0;
        } else if (this.getStartBitPos() < o.getStartBitPos()) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * Returns whether otherBitField overlaps with this BitField.
     * @param otherBitField The other BitField to compare with.
     * @return true if otherBitField overlaps with this BitField; false otherwise.
     */
    public boolean isOverlapping(BitField otherBitField) {
        return (this.getStartBitPos() <= (otherBitField.getEndBitPos()) &&
                otherBitField.getStartBitPos() <= this.getEndBitPos());
    }

    @Override
    public String toString() { return "[" + getStartBitPos() + ", " + getEndBitPos() + "]"; }

    public String getName() { return name; }

    public void setName(String newName) { name = newName; }

    public int getStartBitPos() { return startBitPos; }

    public int getEndBitPos() { return endBitPos; }

    public int getNrBits() { return getEndBitPos() - getStartBitPos() + 1; }

    /**
     * Calculates and returns the field value of the bit field for the given sequenceValue, as a long.
     *
     * @param sequenceValue Bitfield sequence value for which the bitfield value should be calculated.
     * @return The bitfield value, as a long.
     */
    public long getFieldValue(long sequenceValue) {
        final long mask = ((1 << (getEndBitPos() - getStartBitPos() + 1)) - 1) << getStartBitPos();
        final long fieldValue = (sequenceValue & mask) >> getStartBitPos();

        return fieldValue;
    }

    /**
     * Calculates and returns the field value of the bit field for the given sequenceValue, as a BigInteger.
     *
     * @param sequenceValue Bitfield sequence value for which the bitfield value should be calculated.
     * @return The bitfield value, as a BigInteger.
     */
    public BigInteger getFieldValue(BigInteger sequenceValue) {
        // Create mask, e.g. 0000 0000 1111 0000 for BitField(4, 7)
        final BigInteger mask = (BigInteger.ONE
                .shiftLeft(getNrBits()))
                .subtract(BigInteger.ONE)
                .shiftLeft(getStartBitPos());

        // Apply the mask to the sequence value: 0000 0000 xxxx 0000
        final BigInteger shiftedFieldValue = sequenceValue.and(mask);

        // Finally, shift the truncated value to the right to get the field value
        final BigInteger fieldValue = shiftedFieldValue.shiftRight(getStartBitPos());

        return fieldValue;
    }

    /**
     * Applies the given field value to the sequence value.
     * @param fieldValue Field value to apply
     * @param sequenceValue Bit sequence to apply the field value to.
     * @return The new sequence value.
     * @throws java.lang.IllegalArgumentException if fieldValue does not fit in the BitField instance
     * or if sequenceValue consists of fewer bits than fieldValue.
     */
    public BigInteger applyFieldValue(BigInteger fieldValue, BigInteger sequenceValue)
            throws IllegalArgumentException {
        if (fieldValue.bitLength() > getNrBits()) {
            throw new IllegalArgumentException("Field value does not fit in BitField instance");
        }

        // Create mask, e.g. 1111 1111 0000 1111 for BitField(4, 7)
        final BigInteger mask = (BigInteger.ONE
                .shiftLeft(getNrBits()))
                .subtract(BigInteger.ONE)
                .shiftLeft(getStartBitPos())
                .not();

        // Clear bits of field value in sequence value using this mask: xxxx xxxx 0000 xxxx
        final BigInteger clearedSequenceValue = sequenceValue.and(mask);

        // Calculate shifted field value: 0000 0000 yyyy 0000
        final BigInteger shiftedFieldValue = fieldValue.shiftLeft(getStartBitPos());

        // Finally, OR both values to yield the new field value: xxxx xxxx yyyy xxxx
        final BigInteger newSequenceValue = clearedSequenceValue.or(shiftedFieldValue);

        return newSequenceValue;
    }
}
