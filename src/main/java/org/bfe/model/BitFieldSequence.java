package org.bfe.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a sequence of {@link BitField}s.
 */
public class BitFieldSequence implements Serializable {
    private static final long serialVersionUID = 4076506635890231996L;

    private static final int MAX_NR_BITS = 64;

    /**
     * Set of bit fields.
     *
     * The bit fields in this set should always comply to its invariants; see {@link #checkInvariants()}.
     */
    protected List<BitField> bitFields;
    private String name;

    /**
     * Constructor for a zero-bit sequence.
     *
     * @param name Name of the BitFieldSequence.
     */
    public BitFieldSequence(String name) throws IllegalArgumentException {
        this.name = name;
        this.bitFields = createBitFieldList();
    }

    /**
     * Constructor.
     *
     * @param name Name of the BitFieldSequence.
     * @param nrBits Number of bits used by the BitFieldSequence.
     * @throws IllegalArgumentException if the number of bits is out of range.
     */
    public BitFieldSequence(String name, int nrBits) throws IllegalArgumentException {
        this(name);

        if (nrBits < 0 || nrBits > MAX_NR_BITS) {
            throw new IllegalArgumentException("Number of bits is out of range");
        }

        for (int i = 0; i < nrBits; i++) {
            final BitField newBitField = createBitField(i, i);
            bitFields.add(newBitField);
        }
    }

    /**
     * Copy constructor.
     *
     * @param source The source BitFieldSequence.
     */
    public BitFieldSequence(BitFieldSequence source) {
        this(source.name);

        copyBitFields(source.getBitFields());
    }

    /**
     * Copies the given bit fields to this BitFieldSequence.
     *
     * @param sourceBitFields List of BitFields to copy to this BitFieldSequence.
     */
    protected void copyBitFields(List<BitField> sourceBitFields) {
        for (final BitField sourceBitField : sourceBitFields) {
            final BitField newBitField =
                    createBitField(sourceBitField.getStartBitPos(), sourceBitField.getEndBitPos());

            newBitField.setName(sourceBitField.getName());

            bitFields.add(newBitField);
        }
    }

    /**
     * Overridable method for creating a BitField list.
     * @return
     */
    protected List<BitField> createBitFieldList() {
        return new ArrayList<>();
    }

    /**
     * Overridable method for creating a BitField element.
     *
     * @param startBitPos Position of first bit in field.
     * @param endBitPos Position of last bit in field.
     */
    public BitField createBitField(int startBitPos, int endBitPos) {
        return new BitField(startBitPos, endBitPos);
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Combines the bit fields with the given start and end indices(!), inclusive.
     *
     * @param startIndex Zero-based index of the first bit field to combine.
     * @param endIndex Zero-based index of the last bit field to combine.
     * @throws IllegalArgumentException if a bit field does not exist at the given indices.
     */
    public void combine(int startIndex, int endIndex) throws IllegalArgumentException {
        if (startIndex < 0 || endIndex > getNrFields() - 1) {
            throw new IllegalArgumentException("Start or end index is out of field");
        }

        if (startIndex >= endIndex) {
            throw new IllegalArgumentException("Start index is not smaller than end index");
        }

        int startBit = bitFields.get(startIndex).getStartBitPos();
        int endBit = bitFields.get(endIndex).getEndBitPos();

        // Remove all bit fields for the given index field and replace it with a single one
        // covering all bits.
        for (int i = startIndex; i <= endIndex; i++) {
            removeBitField(startIndex);
        }

        addBitField(startIndex, startBit, endBit);

        checkInvariants();
    }

    /**
     * Splits the bit field with the given index into adjacent bit fields, each covering a single
     * bit.
     *
     * @param index Zero-based index of the bit field to split.
     * @throws IllegalArgumentException If the index does not exist or is not splittable
     * because the bit field covers only a single bit.
     */
    public void split(int index) throws IllegalArgumentException {
        if (index < 0 || index > getNrFields() - 1) {
            throw new IllegalArgumentException("Index is out of field");
        }

        int startBit = bitFields.get(index).getStartBitPos();
        int endBit = bitFields.get(index).getEndBitPos();

        if (startBit == endBit) {
            throw new IllegalArgumentException("Bit field at index " + index + " is not " +
                    "splittable");
        }

        removeBitField(index);

        int indexCounter = index;

        // For each bit covered by the removed BitField, create a new one covering only a single bit.
        for (int i = startBit; i <= endBit; i++) {
            addBitField(indexCounter, i, i);
            indexCounter++;
        }

        checkInvariants();
    }

    /**
     * Adds a BitField to the sequence.
     *
     * @param index Zero-based index where the bit field should be added, or -1 to add it at
     * the end of the sequence.
     * @param startBitPos Position of first bit in field.
     * @param endBitPos Position of last bit in field.
     */
    private void addBitField(int index, int startBitPos, int endBitPos) {
        final BitField bitField = createBitField(startBitPos, endBitPos);

        if (index == -1) {
            bitFields.add(bitField);
        } else {
            bitFields.add(index, bitField);
        }
    }

    /**
     * Removes a BitField from the sequence.
     *
     * @param index Zero-based index of the bit field to remove.
     */
    private void removeBitField(int index) {
        bitFields.remove(index);
    }

    /**
     * Returns the start bit position covered by the bit field at the given index.
     *
     * @param index Index (zero-based) of the bit field to return the start bit for.
     * @return The start bit position (zero-based).
     * @throws IllegalArgumentException if a bit field with the given index does not exist.
     */
    public int getStartBitPos(int index) throws IllegalArgumentException {
        if (index < 0 || index > getNrFields() - 1) {
            throw new IllegalArgumentException("Index is out of field");
        }

        return bitFields.get(index).getStartBitPos();
    }

    /**
     * Returns the end bit position covered by the bit field at the given index.
     *
     * @param index Index (zero-based) of the bit field to return the end bit for.
     * @return The end bit position (zero-based).
     * @throws IllegalArgumentException if no bit field exists for the given index.
     */
    public int getEndBitPos(int index) throws IllegalArgumentException {
        if (index < 0 || index > getNrFields() - 1) {
            throw new IllegalArgumentException("Index is out of field");
        }

        return bitFields.get(index).getEndBitPos();
    }

    /**
     * Returns the number of bits in the sequence.
     */
    public int getNrBits() {
        return getEndBitPos(bitFields.size() - 1) + 1;
    }

    /**
     * Returns the number of bit fields in the sequence.
     */
    public int getNrFields() {
        return bitFields.size();
    }

    /**
     * Checks whether the BitFieldSequence complies to its invariants (rules), which are:
     *
     * <ul>
     *     <li>For each i where 0 < i < bitFields.size():<br>
     *     BitField(i).endBitPos < BitField(i).startBitPos</li>
     * </ul>
     * @throws java.lang.IllegalStateException if an invariant is violated.
     */
    private void checkInvariants() throws IllegalStateException {
        final int index = findOverlapping(this.bitFields);
        if (index > 0) {
            throw new IllegalStateException("Bit field field[" + (index-1) +
                    "] overlaps with bit field field[" + index + "]");
        }
    }

    /**
     * Finds the first index whose BitField overlaps with the previous one.
     *
     * @return the first overlapping index, or zero if no overlapping index was found.
     */
    private int findOverlapping(List<BitField> bitFields) {
        final Iterator<BitField> bitFieldIterator = bitFields.iterator();

        BitField curBitField = bitFieldIterator.next();
        while (bitFieldIterator.hasNext()) {
            BitField prevBitField = curBitField;
            curBitField = bitFieldIterator.next();

            if (prevBitField.isOverlapping(curBitField)) {
                return prevBitField.getStartBitPos();
            }
        }

        return 0;
    }

    /**
     * Returns the list of bit fields currently defined in this BitFieldSequence.
     *
     * @return A list of {@link BitField}s.
     */
    public List<BitField> getBitFields() {
        return bitFields;
    }
}
