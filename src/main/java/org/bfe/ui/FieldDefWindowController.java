package org.bfe.ui;

import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.bfe.model.BitField;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class FieldDefWindowController implements Initializable {
    private final Stage stage;
    private final BitFieldSequenceWrapper bitFieldSequence;
    private boolean isCancelled = true;

    @FXML
    private TextField name;

    @FXML
    private TextField nrBits;

    @FXML
    private TableView<BitField> tableFields;

    @FXML
    private TableColumn<BitField, String> bitFieldColumn;

    @FXML
    private TableColumn<BitField, String> fieldNameColumn;

    @FXML
    public Button btnCombine;

    @FXML
    public Button btnSplit;

    @FXML
    public Button btnSave;


    public FieldDefWindowController(BitFieldSequenceWrapper bitFieldSequence) {
        this.bitFieldSequence = bitFieldSequence;

        this.stage = new Stage();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        name.textProperty().bindBidirectional(bitFieldSequence.nameProperty());

        nrBits.setText(String.valueOf(bitFieldSequence.getNrBits()));

        tableFields.setItems(bitFieldSequence.getObservableList());
        tableFields.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        // Set cell value factories to columns in order to bind them to the BitFieldWrappers.
        bitFieldColumn.setCellValueFactory(new PropertyValueFactory<BitField, String>(BitFieldWrapper.BIT_RANGE_PROPERTY_NAME));
        fieldNameColumn.setCellValueFactory(new PropertyValueFactory<BitField, String>(BitFieldWrapper.RANGE_NAME_PROPERTY_NAME));

        // Make field name column editable
        fieldNameColumn.setCellFactory(TextFieldTableCell.<BitField>forTableColumn());

        tableFields.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<BitField>() {
            @Override
            public void onChanged(Change<? extends BitField> change) {
                // Enable Combine button if the user selected more than one item.
                final boolean enableCombine = (change.getList().size() > 1);

                // Enable Split button if the user selected only one BitField which is bigger than one
                // bit position.
                final boolean enableSplit;
                if (change.getList().size() == 1) {
                    BitField selectedBitField = change.getList().get(0);
                    enableSplit = (selectedBitField.getStartBitPos() != selectedBitField.getEndBitPos());
                } else {
                    enableSplit = false;
                }

                btnCombine.setDisable(!enableCombine);
                btnSplit.setDisable(!enableSplit);
            }
        });

        btnSave.disableProperty().bind(name.textProperty().isEqualTo((String)null));
    }

    /**
     * Shows a stage (dialog) for editing the bit field sequence that was passed to the constructor.
     * @return The edited bit field sequence, or null if the dialog was cancelled.
     * @throws IOException if the stage could not be loaded.
     */
    public BitFieldSequenceWrapper editFieldDef() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("FieldDefWindow.fxml"));
        loader.setController(this);

        Parent parent = (Parent)loader.load();

        Scene newDialog = new Scene(parent);

        stage.setScene(newDialog);
        stage.initModality(Modality.APPLICATION_MODAL);

        stage.showAndWait();

        if (isCancelled()) {
            return null;
        } else {
            return bitFieldSequence;
        }
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    @FXML
    public void onTotalNrBitsKeyPressed(KeyEvent event) {
    }

    @FXML
    public void onCombine(ActionEvent actionEvent) {
        List<Integer> selectedIndices = tableFields.getSelectionModel().getSelectedIndices();

        // Check that the selection is continuous.
        int count = 0;
        while (count < (selectedIndices.size() - 1)) {
            int currentIndex = selectedIndices.get(count);
            int nextIndex = selectedIndices.get(count+1);
            if (nextIndex != (currentIndex + 1)) {
                Dialogs.showErrorDialog(stage, "Please select a continuous range.");
                return;
            }
            count++;
        }

        int firstSelectionIndex = selectedIndices.get(0);
        int lastSelectionIndex = selectedIndices.get(selectedIndices.size() - 1);

        if (firstSelectionIndex == lastSelectionIndex) {
            Dialogs.showErrorDialog(stage, "Please select at least two elements.");
            return;
        }

        bitFieldSequence.combine(firstSelectionIndex, lastSelectionIndex);
    }

    @FXML
    public void onSplit(ActionEvent actionEvent) {
        int selectedIndex = tableFields.getSelectionModel().getSelectedIndex();

        bitFieldSequence.split(selectedIndex);
    }

    @FXML
    public void onSave(ActionEvent actionEvent) {
        isCancelled = false;
        stage.close();
    }

    @FXML
    public void onCancel(ActionEvent actionEvent) {
        isCancelled = true;
        stage.close();
    }

}
