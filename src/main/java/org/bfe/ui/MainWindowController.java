package org.bfe.ui;

import javafx.beans.binding.StringBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.bfe.XmlStorage;
import org.bfe.model.BitField;
import org.bfe.model.BitFieldSequence;

import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainWindowController implements Initializable {
    public static final Integer BITFIELD_SIZE_CHOICES[] = { 16, 32, 64 };
    private static final int HEX_RADIX = 16;
    private static final int DECIMAL_RADIX = 10;

    private Stage stage;
    private String defsFileName;
    private int radix = DECIMAL_RADIX;

    @FXML
    private ComboBox<BitFieldSequenceWrapper> comboFieldDef;

    @FXML
    private RadioButton btnHex;

    // TODO: Put totalMinValue and totalMaxValue into a single label?
    @FXML
    private Label totalMinValue;

    @FXML
    private Label totalMaxValue;

    @FXML
    private TextField totalValueText;

    @FXML
    private TableView<BitField> tableFields;

    @FXML
    private TableColumn<BitField, String> bitFieldColumn;

    @FXML
    private TableColumn<BitField, String> fieldNameColumn;

    @FXML
    private TableColumn<BitField, String> valueColumn;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initFieldDefCombo();
        initRadix();
        initLabels();
        initTotalValue();
        initTable();
    }

    private void initFieldDefCombo() {
        populateFieldDefs();
        
        comboFieldDef.setCellFactory(new Callback<ListView<BitFieldSequenceWrapper>,
                ListCell<BitFieldSequenceWrapper>>() {
            @Override
            public ListCell<BitFieldSequenceWrapper> call(ListView<BitFieldSequenceWrapper> bitFieldSequenceListView) {
                return new ListCell<BitFieldSequenceWrapper>() {
                    @Override
                    public void updateItem(BitFieldSequenceWrapper sequence, boolean empty) {
                        super.updateItem(sequence, empty);

                        if (empty) {
                            setText(null);
                        } else {
                            setText(getSequenceAsString(sequence));
                        }
                    }
                };
            }
        });

        comboFieldDef.setConverter(new StringConverter<BitFieldSequenceWrapper>() {
            @Override
            public String toString(BitFieldSequenceWrapper sequence) {
                return getSequenceAsString(sequence);
            }

            @Override
            public BitFieldSequenceWrapper fromString(String s) {
                // Method should not be called, since comboFieldDef is not editable.
                throw new UnsupportedOperationException();
            }
        });

        comboFieldDef.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<BitFieldSequenceWrapper>() {
            @Override
            public void changed(ObservableValue<? extends BitFieldSequenceWrapper> observableValue,
                                BitFieldSequenceWrapper oldValue,
                                BitFieldSequenceWrapper newValue) {
                if (newValue != null) {
                    tableFields.setItems(newValue.getObservableList());
                } else {
                    tableFields.setItems(null);
                }
            }
        });
    }

    private void populateFieldDefs() {
        XmlStorage xmlStorage = new XmlStorage();
        try {
            final InputStream is = new FileInputStream(this.defsFileName);
            final List<BitFieldSequence> bfsList = xmlStorage.load(is);

            final ObservableList<BitFieldSequenceWrapper> bfsWrapperList = FXCollections.observableArrayList();
            for (BitFieldSequence bfs : bfsList) {
                final BitFieldSequenceWrapper bfsWrapper = new BitFieldSequenceWrapper(bfs);
                bfsWrapperList.add(bfsWrapper);
            }

            this.comboFieldDef.setItems(bfsWrapperList);
            this.comboFieldDef.getSelectionModel().select(bfsWrapperList.size()-1);
        } catch (FileNotFoundException ex) {
            // Ignore, definitions file may not be present when running for the first time.
        } catch (RuntimeException ex) {
            Dialogs.showErrorDialog(this.stage, "Could not load field definitions from file " + defsFileName +
                    " (see console output).");
            ex.printStackTrace(); // TODO: Write to application log.
        }
    }

    private String getSequenceAsString(BitFieldSequenceWrapper sequence) {
        return sequence.getName() + " (" + sequence.getNrBits() + "-bit field)";
    }

    private void initRadix() {
        btnHex.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observableValue, Boolean oldSelectionValue,
                                Boolean newSelectionValue) {
                int oldRadix = radix;
                if (newSelectionValue) {
                    radix = HEX_RADIX;
                } else {
                    radix = DECIMAL_RADIX;
                }

                if (!totalValueText.getText().isEmpty()) {
                    final BigInteger numericValue = new BigInteger(totalValueText.getText(), oldRadix);
                    totalValueText.setText(numericValue.toString(radix));
                }
            }
        });
    }

    private void initLabels() {
        totalMaxValue.textProperty().bind(new MaxValueLabelStringBinding());
    }

    private void initTotalValue() {
        // Disable the total value textbox if there is no selection in the field def combo.
        totalValueText.disableProperty().bind(comboFieldDef.getSelectionModel().selectedItemProperty().isNull());

        // Restrict the total value to numbers only (hex / decimal).
        totalValueText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                                String oldValue, String newValue) {
                try {
                    // The check on non-empty below is necessary to prevent a stack overflow.
                    // When an invalid first character is entered, this will trigger a NumberFormatException.
                    // In turn, this will cause the text to be reset to an empty string, which triggers this method
                    // again. Without the if below, an empty string would also trigger a NumberFormatException.
                    if (!newValue.isEmpty()) {
                        BigInteger value = new BigInteger(newValue, radix);
                        final BitFieldSequenceWrapper selectedSequence = comboFieldDef.getSelectionModel().getSelectedItem();
                        // Reset the total value if the new value is negative or if it doesn't fit in the
                        // current field definition.
                        if (value.signum() < 0 || value.bitLength() > selectedSequence.getNrBits()) {
                            totalValueText.setText(oldValue);
                            // TODO: Display error message (e.g. in a status label)?
                        }
                    }
                } catch (NumberFormatException e) {
                    totalValueText.setText(oldValue);
                }
            }
        });

    }

    private void initTable() {
        tableFields.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        // Set cell value factories to columns in order to bind them to the BitFieldWrappers.
        bitFieldColumn.setCellValueFactory(new PropertyValueFactory<BitField, String>(BitFieldWrapper.BIT_RANGE_PROPERTY_NAME));
        fieldNameColumn.setCellValueFactory(new PropertyValueFactory<BitField, String>(BitFieldWrapper.RANGE_NAME_PROPERTY_NAME));

        valueColumn.setCellFactory(TextFieldTableCell.<BitField>forTableColumn());
        valueColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<BitField, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<BitField, String> bitFieldStringCellEditEvent) {
                final String totalValueText = MainWindowController.this.totalValueText.getText();
                final BigInteger oldTotalValue;
                if (totalValueText.isEmpty()) {
                    oldTotalValue = BigInteger.ZERO;
                } else {
                    oldTotalValue = new BigInteger(totalValueText, radix);
                }

                final BigInteger fieldValue = new BigInteger(bitFieldStringCellEditEvent.getNewValue(), radix);
                final BitField selectedBitField = bitFieldStringCellEditEvent.getRowValue();

                try {
                    // TODO: Determining newTotalValue should really take place via properties, e.g.
                    // a totalValue property in BitFieldSequenceWrapper that is bound (using bindBidirectional())
                    // to a fieldValue property in the BitFieldWrapper instances.
                    if (fieldValue.signum() > -1) { // Don't allow negative field values for now
                        final BigInteger newTotalValue = selectedBitField.applyFieldValue(fieldValue, oldTotalValue);
                        MainWindowController.this.totalValueText.setText(newTotalValue.toString(radix));
                    } else {
                        MainWindowController.this.totalValueText.setText(totalValueText);
                    }
                } catch (IllegalArgumentException ex) {
                    // Entered value was invalid -> reset text
                    // TODO: Resetting the text of the table cell seems to be non-trivial
                    // (Google: "javafx setOnEditCommit revert"). So instead we just reset
                    // totalValueText, which triggers an update of the edited cell and reverts it.
                    MainWindowController.this.totalValueText.setText(totalValueText);
                }
            }
        });

        valueColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<BitField, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(final TableColumn.CellDataFeatures<BitField, String> bitFieldStringCellDataFeatures) {
                return new StringBinding() {
                    {
                        // This way, the cell values will be updated when the value of totalValueText or the hex button
                        // selection changes.
                        super.bind(totalValueText.textProperty(), btnHex.selectedProperty());
                    }

                    @Override
                    protected String computeValue() {
                        BigInteger sequenceValue = BigInteger.ZERO;
                        try {
                            sequenceValue = new BigInteger(totalValueText.getText(), radix);
                        } catch (NumberFormatException ex) {
                            // Do nothing, sequenceValue remains zero.
                        }
                        BigInteger fieldValue = bitFieldStringCellDataFeatures.getValue().getFieldValue(sequenceValue);
                        return fieldValue.toString(radix);
                    }
                };
            }
        });
    }

    @FXML
    public void onTotalValueKeyPressed(KeyEvent event) {
    }

    @FXML
    public void onNewFieldDef(ActionEvent actionEvent) throws IOException {
        Integer choice = Dialogs.showInputDialog(stage, "Please specify the size of the bit field:",
                "Enter size", "Bit Field Size", BITFIELD_SIZE_CHOICES[0], BITFIELD_SIZE_CHOICES);

        if (choice != null) {
            BitFieldSequenceWrapper bitFieldSequence = new BitFieldSequenceWrapper("New Bitfield", choice.intValue());
            FieldDefWindowController controller = new FieldDefWindowController(bitFieldSequence);

            // editFieldDef() blocks until the user closes the dialog.
            BitFieldSequenceWrapper sequence = controller.editFieldDef();

            if (sequence != null) {
                comboFieldDef.getItems().add(sequence);
                comboFieldDef.getSelectionModel().select(sequence);
            }
        }
    }

    @FXML
    public void onEditFieldDef(ActionEvent actionEvent) throws IOException {
        if (comboFieldDef.getSelectionModel().isEmpty()) {
            Dialogs.showErrorDialog(stage, "No field definition is selected.");
            return;
        }
        
        // Create a copy of the sequence in order to prevent modifications to the original if the user chooses to
        // cancel editing.
        BitFieldSequenceWrapper bfsCopy = new BitFieldSequenceWrapper(comboFieldDef.getValue());

        FieldDefWindowController controller = new FieldDefWindowController(bfsCopy);

        // editFieldDef() blocks until the user closes the dialog.
        BitFieldSequenceWrapper editedSequence = controller.editFieldDef();

        if (editedSequence != null) {
            // Replace the sequence that was chosen from the combo box by the edited sequence.
            final int selectedIndex = comboFieldDef.getSelectionModel().getSelectedIndex();

            comboFieldDef.getItems().set(selectedIndex, editedSequence);

            // Set the selection again, as the set() method above resets it.
            comboFieldDef.getSelectionModel().select(selectedIndex);
        }
    }

    @FXML
    public void onDeleteFieldDef(ActionEvent actionEvent) throws IOException {
        if (comboFieldDef.getSelectionModel().isEmpty()) {
            Dialogs.showErrorDialog(stage, "No field definition is selected.");
            return;
        }
        
        final int selectedIndex = comboFieldDef.getSelectionModel().getSelectedIndex();

        comboFieldDef.getItems().remove(selectedIndex);
    }

    /**
     * Loads the the main window and shows it.
     * @param primaryStage The primary stage to use for this window.
     * @param defsFileName File name to load/save bitfield sequence definitions.
     * @throws IOException if the window could not be loaded.
     */
    public void showMainWindow(Stage primaryStage, String defsFileName) throws IOException {
        this.stage = primaryStage;
        this.defsFileName = defsFileName;

        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"));
        loader.setController(this);

        Parent root = (Parent)loader.load();

        stage.setTitle("Bit Field Editor");
        stage.setScene(new Scene(root));
        stage.setResizable(false);

        stage.show();
    }

    public void onClose(ActionEvent actionEvent) {
        storeFieldDefs();

        stage.close();
    }

    private void storeFieldDefs() {
        XmlStorage xmlStorage = new XmlStorage();
        try {
            final OutputStream os = new FileOutputStream(this.defsFileName);
            List<BitFieldSequenceWrapper> bfsWrapperList = this.comboFieldDef.getItems();
            List<BitFieldSequence> bfsList = new ArrayList<>();

            for (BitFieldSequenceWrapper bfsWrapper : bfsWrapperList) {
                BitFieldSequence bfs = new BitFieldSequence(bfsWrapper);
                bfsList.add(bfs);
            }

            xmlStorage.store(bfsList, os);

        } catch (FileNotFoundException ex) {
            Dialogs.showErrorDialog(this.stage, "Could not store field definitions in file " + defsFileName +
                " (see console output).");
            ex.printStackTrace(); // TODO: Write to application log.
        } catch (RuntimeException ex) {
            Dialogs.showErrorDialog(this.stage, "Could not store field definitions in file " + defsFileName +
                " (see console output).");
            ex.printStackTrace(); // TODO: Write to application log.
        }
    }

    /**
     * String binding for maximum value label.
     */
    private class MaxValueLabelStringBinding extends StringBinding {
        // Bind the label to both the field def selection and the radix selection.
        {
            super.bind(comboFieldDef.getSelectionModel().selectedItemProperty(),
                       btnHex.selectedProperty());
        }

        @Override
        protected String computeValue() {
            final int nrBits;
            final BitFieldSequenceWrapper selectedFieldDef =
                    comboFieldDef.getSelectionModel().selectedItemProperty().getValue();
            if (selectedFieldDef != null) {
                nrBits = selectedFieldDef.getNrBits();
            } else {
                nrBits = 0;
            }

            // Equivalent to: maxValue = (1 << nrBits) - 1);
            final BigInteger maxValue = BigInteger.ONE.shiftLeft(nrBits).subtract(BigInteger.ONE);

            return maxValue.toString(btnHex.isSelected() ? HEX_RADIX : DECIMAL_RADIX);
        }
    }

}
