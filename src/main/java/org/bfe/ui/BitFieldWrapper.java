package org.bfe.ui;

import javafx.beans.property.Property;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.adapter.JavaBeanStringProperty;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import org.bfe.model.BitField;

/**
 * Created by iwan on 6/11/14.
 */
public class BitFieldWrapper extends BitField {
    public static final String BIT_RANGE_PROPERTY_NAME = "bitRange";
    public static final String RANGE_NAME_PROPERTY_NAME = "rangeName";

    private final ReadOnlyStringWrapper bitRangeProperty;
    private final JavaBeanStringProperty rangeNameProperty;

    /**
     * Constructor.
     *
     * @param startBitPos Position of first bit in range.
     * @param endBitPos Position of last bit in range.
     * @throws IllegalArgumentException if startPos is greater than endPos.
     */
    public BitFieldWrapper(int startBitPos, int endBitPos) {
        super(startBitPos, endBitPos);

        this.bitRangeProperty = new ReadOnlyStringWrapper(toString());
        try {
            this.rangeNameProperty = JavaBeanStringPropertyBuilder.create()
                    .name("name")
                    .bean(this)
                    .build();
        } catch (NoSuchMethodException e) {
            // This should never happen. Convert the exception into an unchecked exception.
            throw new RuntimeException(e);
        }
    }

    public Property<String> bitRangeProperty() {
        return bitRangeProperty;
    }

    public Property<String> rangeNameProperty() {
        return rangeNameProperty;
    }

}
