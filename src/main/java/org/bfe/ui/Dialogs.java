package org.bfe.ui;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceDialog;
import javafx.stage.Stage;

public class Dialogs {

    public static void showErrorDialog(Stage stage, String string) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.initOwner(stage);
        alert.setTitle("Error");
        //alert.setHeaderText("Look, an Error Dialog");
        alert.setContentText(string);

        alert.showAndWait();
    }

    public static <T> T showInputDialog(Stage stage, String prompt, String header, String title, T defaultChoice,
            T[] choices) {
        ChoiceDialog<T> dialog = new ChoiceDialog<T>(defaultChoice, choices);
        dialog.initOwner(stage);
        dialog.setTitle(title);
        dialog.setHeaderText(header);
        dialog.setContentText(prompt);
    
        // Traditional way to get the response value.
        Optional<T> result = dialog.showAndWait();
        if (result.isPresent()){
            return result.get();
        } else {
            return null;
        }
    }
}
