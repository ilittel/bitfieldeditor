package org.bfe.ui;

import javafx.beans.property.Property;
import javafx.beans.property.adapter.JavaBeanStringProperty;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.bfe.model.BitField;
import org.bfe.model.BitFieldSequence;

import java.util.List;

/**
 * JavaFx wrapper class for BitFieldSequence.
 */
public class BitFieldSequenceWrapper extends BitFieldSequence {
    private final JavaBeanStringProperty nameProperty;

    /**
     * Constructor.
     *
     * @param name Name of the BitFieldSequence.
     * @param nrBits Number of bits used by the BitFieldSequence.
     * @throws IllegalArgumentException if the number of bits is out of range.
     */
    public BitFieldSequenceWrapper(String name, int nrBits) throws IllegalArgumentException {
        super(name, nrBits);

        try {
            this.nameProperty = JavaBeanStringPropertyBuilder.create()
                    .name("name")
                    .bean(this)
                    .build();
        } catch (NoSuchMethodException e) {
            // This should never happen. Convert the exception into an unchecked exception.
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates a BitFieldSequenceWrapper instance by creating a COPY of the given BitFieldSequence.
     * @param bitFieldSequence The BitFieldSequence to copy.
     */
    public BitFieldSequenceWrapper(BitFieldSequence bitFieldSequence) {
        super(bitFieldSequence);

        try {
            this.nameProperty = JavaBeanStringPropertyBuilder.create()
                    .name("name")
                    .bean(this)
                    .build();
        } catch (NoSuchMethodException e) {
            // This should never happen. Convert the exception into an unchecked exception.
            throw new RuntimeException(e);
        }
    }

    /**
     * @return the BitFieldSequence's name property.
     */
    public Property<String> nameProperty() {
        return nameProperty;
    }

    @Override
    public List<BitField> createBitFieldList() {
        return FXCollections.observableArrayList();
    }

    public ObservableList<BitField> getObservableList() {
        return (ObservableList<BitField>)getBitFields();
    }

    @Override
    public BitField createBitField(int startBitPos, int endBitPos) {
        return new BitFieldWrapper(startBitPos, endBitPos);
    }
}
