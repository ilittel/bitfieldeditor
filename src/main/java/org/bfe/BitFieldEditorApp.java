package org.bfe;

import javafx.application.Application;
import javafx.stage.Stage;
import org.bfe.ui.MainWindowController;

public class BitFieldEditorApp extends Application {
    private static final String DEFAULT_DEFINITIONS_FILE_NAME = "definitions.xml";

    @Override
    public void start(Stage primaryStage) throws Exception{
        MainWindowController controller = new MainWindowController();

        final Parameters cmdLineArgs = getParameters();

        final String defsFileName;
        if (cmdLineArgs.getRaw().size() > 0) {
            defsFileName = cmdLineArgs.getRaw().get(0);
        } else {
            defsFileName = DEFAULT_DEFINITIONS_FILE_NAME;
        }

        controller.showMainWindow(primaryStage, defsFileName);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
