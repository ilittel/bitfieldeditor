package org.bfe;

import com.thoughtworks.xstream.XStream;
import org.bfe.model.BitField;
import org.bfe.model.BitFieldSequence;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Persists BitFieldSequences to/from XML.
 */
public class XmlStorage {
    private final XStream xStream;

    public XmlStorage() {
        xStream = new XStream();

        xStream.alias("BitFieldSequence", BitFieldSequence.class);
        xStream.alias("BitField", BitField.class);
    }

    public List<BitFieldSequence> load(InputStream source) throws RuntimeException {
        List<BitFieldSequence> bfsList = (List<BitFieldSequence>) xStream.fromXML(source);
        return bfsList;
    }

    public void store(List<BitFieldSequence> bfsList, OutputStream destination) throws RuntimeException {
        xStream.toXML(bfsList, destination);
    }
}
