package org.bfe.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by iwan on 12/23/13.
 */
public class BitFieldSequenceTest {
    private BitFieldSequence model;

    // ************************************* Initialization *************************************

    @Before
    public void initialize() {
        model = new BitFieldSequence(null, 16);
    }

    // ************************************* combine() tests *************************************

    @Test()
    public void testCombineSplitHappyFlow() {
        model.combine(0, 12);
        Assert.assertEquals(4, model.getNrFields());

        Assert.assertEquals(0,model.getStartBitPos(0));
        Assert.assertEquals(12,model.getEndBitPos(0));
        Assert.assertEquals(13,model.getStartBitPos(1));
        Assert.assertEquals(13,model.getEndBitPos(1));
        Assert.assertEquals(14,model.getStartBitPos(2));
        Assert.assertEquals(14,model.getEndBitPos(2));
        Assert.assertEquals(15,model.getStartBitPos(3));
        Assert.assertEquals(15,model.getEndBitPos(3));

        model.split(0);
        Assert.assertEquals(16, model.getNrFields());
        for (int i = 0; i < 16; i++) {
            Assert.assertEquals(i, model.getStartBitPos(i));
            Assert.assertEquals(i, model.getEndBitPos(i));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCombineStartIndexTooLarge() {
        model.combine(16, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCombineEndIndexTooLarge() {
        model.combine(0, 17);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCombineEndIndexTooLarge2() {
        model.combine(0, 6);

        Assert.assertEquals(6, model.getEndBitPos(0));
        Assert.assertEquals(7, model.getStartBitPos(1));

        // Bit range zero now covers bits 0..6, so now there are only 10 bit ranges.
        Assert.assertEquals(10, model.getNrFields());

        model.combine(0, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCombineStartIndexTooSmall() {
        model.combine(-1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCombineEndIndexSmallerThanStart() {
        model.combine(15, 14);
    }

    // ************************************* split() tests *************************************

    @Test(expected = IllegalArgumentException.class)
    public void testSplitIndexTooLarge() {
        model.split(16);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSplitIndexTooLarge2() {
        model.combine(0, 10);

        Assert.assertEquals(10, model.getEndBitPos(0));
        Assert.assertEquals(11, model.getStartBitPos(1));

        // Bit range zero now covers bits 0..10, so now there are only 6 bit ranges.
        Assert.assertEquals(6, model.getNrFields());

        model.split(7);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSplitIndexTooSmall() {
        model.split(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSplitNotSplittable() {
        model.split(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSplitNotSplittable2() {
        model.combine(0, 10);

        // Bit range zero now covers bits 0..10, so bit range [1] only covers bit 11.
        Assert.assertEquals(11, model.getStartBitPos(1));
        Assert.assertEquals(11, model.getEndBitPos(1));

        model.split(1);
    }

    // *********************************** getStartBitPos() tests ***********************************

    @Test(expected = IllegalArgumentException.class)
    public void testGetStartBitIndexTooSmall() {
        model.getStartBitPos(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStartBitIndexTooSmall2() {
        model.combine(0, 5);

        // Bit range zero now covers bits 0..5, so now there are only 11 bit ranges.

        model.getStartBitPos(11);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetStartBitIndexTooBig() {
        model.getStartBitPos(16);
    }

    // *********************************** getEndBitPos() tests ***********************************

    @Test(expected = IllegalArgumentException.class)
    public void testGetEndBitIndexTooSmall() {
        model.getEndBitPos(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetEndBitIndexTooSmall2() {
        model.combine(0, 5);

        // Bit range zero now covers bits 0..5, so now there are only 11 bit ranges.

        model.getEndBitPos(11);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetEndBitIndexTooBig() {
        model.getEndBitPos(16);
    }

}
