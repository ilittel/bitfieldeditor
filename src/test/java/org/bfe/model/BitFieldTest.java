package org.bfe.model;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

/**
 * Unit tests for BitField class.
 */
public class BitFieldTest {
    @Test()
    public void testGetFieldValueLong() {
        BitField bitField = new BitField(4, 7);

        Assert.assertEquals(2L, bitField.getFieldValue(33L));
    }

    @Test()
    public void testGetFieldValueBigInteger() {
        BitField bitField = new BitField(4, 7);

        Assert.assertEquals(BigInteger.valueOf(2L), bitField.getFieldValue(BigInteger.valueOf(33L)));
    }


    @Test()
    public void testApplyFieldValueBigInteger() {
        BitField bitField = new BitField(4, 7);

        Assert.assertEquals(BigInteger.valueOf(33L),
                bitField.applyFieldValue(BigInteger.valueOf(2L), BigInteger.valueOf(33L)));

        Assert.assertEquals(BigInteger.valueOf(33L),
                bitField.applyFieldValue(BigInteger.valueOf(2L), BigInteger.valueOf(1L)));
    }

}
