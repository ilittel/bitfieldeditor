# BitFieldEditor

A simple bit field viewer/editor, written in Java using the JavaFx GUI framework.

## Features
* Ability to define custom bit fields by combining individual bits into sequences.
* Entering a value for a bit field updates the value for the sequence parts and vice versa.
* Hex and decimal modes (changeable on the fly).
* Custom bit fields are loaded/saved on startup/shutdown.

## Changelog
* v0.2 (2018-08-16):
    * Removed JavaFx Dialogs lib (now part of JDK 8)
    * Updated build environment to Gradle v4
    * Fixed more bugs
    * Pushed repo to bitbucket
* v0.1.1 (2014-09-17)
    * Added loading/saving bitfield definitions
    * Various GUI code changes
    * Bugfixes
* v0.1 (2014-01-08):
    * Initial version (for Java 7)
  
---
Author: Iwan Littel  
License: MIT
